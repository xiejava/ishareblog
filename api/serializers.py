# -*- coding: utf-8 -*-
"""
    :author: XieJava
    :url: http://ishareread.com
    :copyright: © 2021 XieJava <xiejava@ishareread.com>
    :license: MIT, see LICENSE for more details.
"""
from blog.models import *
from django.contrib.auth.models import User
from rest_framework import serializers


class UserModelSerializer(serializers.ModelSerializer):
    userrole = serializers.SerializerMethodField()

    def get_userrole(self, obj):
        userrole = UserRole.objects.filter(user=obj).first()
        userroleid = None
        if userrole is not None:
            userroleid=userrole.role.id
        #return UserRoleModelSerializer(userrole, many=False).data
        return userroleid

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = super().create(validated_data)
        if password:
            instance.set_password(password)
            instance.save()
        return instance

    class Meta:
        model = User
        fields = ['id', 'username','email', 'userrole', 'is_active']


class BlogCategoryModelSerializer(serializers.ModelSerializer):
    create_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    update_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)

    class Meta:
        model = BlogCategory
        fields = "__all__"


class TagModelSerializer(serializers.ModelSerializer):
    create_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    update_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)

    class Meta:
        model = Tag
        fields = "__all__"


class BlogPostModelSerializer(serializers.ModelSerializer):
    create_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    update_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    pubTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    category_id = serializers.IntegerField(write_only=True)  # 嵌套序列化，用于输入
    category = BlogCategoryModelSerializer(read_only=True)  # 嵌套序列化,设为只读,用于输出
    tags_data = serializers.ListField(write_only=True)  # 多对多，嵌套序列化，用于输入
    tags = TagModelSerializer(many=True, read_only=True)  # 多对多，嵌套序列化，用于输出

    # 多对多，钩子函数序列化,必须是以get_开头的
    def get_tags(self, obj):
        tags = obj.tags.all()
        tag = TagModelSerializer(tags, many=True)
        return tag.data

    def create(self, validated_data):
        category_id = validated_data.pop('category_id', None)
        tags_data = validated_data.pop('tags_data', [])
        category = BlogCategory.objects.get(id=category_id)
        # 创建BlogPost实例
        blog_post = BlogPost.objects.create(category=category, **validated_data)
        blog_post.tags.set(tags_data)
        return blog_post

    def update(self, instance, validated_data):
        category_id = validated_data.pop('category_id', None)
        tags_data = validated_data.pop('tags_data', [])
        if category_id:
            category = BlogCategory.objects.get(id=category_id)
            instance.category = category
        if tags_data:
            instance.tags.set(tags_data)
        super().update(instance, validated_data)
        return instance

    class Meta:
        model = BlogPost
        fields = '__all__'


class SiteModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Site
        fields = "__all__"


class SocialModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Social
        fields = "__all__"


class FocusModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Focus
        fields = "__all__"


class FriendModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Friend
        fields = "__all__"


class MenuModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Menu
        fields = "__all__"

class UserRoleModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserRole
        fields = "__all__"

class RoleModelSerializer(serializers.ModelSerializer):
    menu = MenuModelSerializer(many=True,read_only=True)

    def get_menu(self, obj):
        menus = obj.menu.all()
        menu = MenuModelSerializer(menus, many=True)
        return menu.data
    class Meta:
        model = Role
        fields = "__all__"

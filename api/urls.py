# -*- coding: utf-8 -*-
"""
    :author: XieJava
    :url: http://ishareread.com
    :copyright: © 2021 XieJava <xiejava@ishareread.com>
    :license: MIT, see LICENSE for more details.
"""
from api import views
from django.urls import path
from common.customJWTview import CustomTokenObtainPairView
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)

blogcategory_list = views.BlogCategoryViewset.as_view({'get': 'list', 'post': 'create'})
blogcategory_detail = views.BlogCategoryViewset.as_view({'get': 'retrieve', 'delete': 'destroy', 'put': 'update'})
blog_list = views.BlogsView.as_view({'get': 'list', 'post': 'create'})
blog_detail = views.BlogsView.as_view({'get': 'retrieve','delete': 'destroy', 'put': 'update', 'patch': 'partial_update'})
site_list = views.SiteView.as_view({'get': 'list', })
site_detail = views.SiteView.as_view({'get': 'retrieve', })
social_list = views.SocialView.as_view({'get': 'list', })
social_detail = views.SocialView.as_view({'get': 'retrieve', })
focus_list = views.FocusView.as_view({'get': 'list', })
focus_detail = views.FocusView.as_view({'get': 'retrieve'})
friend_list = views.FriendView.as_view({'get': 'list', })
friend_detail = views.FriendView.as_view({'get': 'retrieve'})
tags_list = views.TagView.as_view({'get': 'list', 'post':'create'})
tags_detail = views.TagView.as_view({'delete': 'destroy'})
user_list = views.UserViewset.as_view({'get': 'list', 'post': 'create'})
user_detail = views.UserViewset.as_view({'get': 'retrieve', 'delete': 'destroy', 'patch': 'partial_update'})
menu_list = views.MenuView.as_view({'get': 'list', })
role_detail = views.RoleView.as_view({'get': 'retrieve'})

# router=DefaultRouter()
# router.register('blogs',views.BlogsView)
urlpatterns = [
    path('category/', blogcategory_list),
    path('category/<pk>', blogcategory_detail),
    path('post/list', blog_list),
    path('post/<pk>', blog_detail),
    path('social/', social_list),
    path('site/<pk>', site_detail),
    path('focus/list', focus_list),
    path('comment/', blog_list),
    path('friend/', friend_list),
    path('tags/', tags_list),
    path('tags/<pk>', tags_detail),
    path('user/', user_list),
    path('user/<pk>', user_detail),
    path('menu/', menu_list),
    path('role/<pk>', role_detail),
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]

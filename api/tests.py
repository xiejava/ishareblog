import requests
import pytest

host = "http://localhost:8000"


class TestApi:
    def test_getcategory_list(self):
        url = f'{host}/api/category/'
        response = requests.get(url)
        assert response.status_code == 200, f'Expected status code 200 but got {response.status_code}'
        assert response.json() != None, f'Expected to get json response but got {response.text}'
        print(response.json())

    def test_getpost_list(self):
        url = f'{host}/api/post/list'
        response = requests.get(url)
        assert response.status_code == 200, f'Expected status code 200 but got {response.status_code}'
        assert response.json() != None, f'Expected to get json response but got {response.text}'


if __name__ == '__main__':
    pytest.main(["-s", "-v", "-p", "no:warnings", "--tb=short", "--html=report.html", "api/tests.py"])

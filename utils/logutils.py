import logging.config
import os
# logging.config.fileConfig("logging.conf")
absolute_path=os.path.abspath(__file__)
log_directory = os.path.dirname(os.path.dirname(absolute_path))
# 日志文件记录在上级目录的logs目录下
logfile = os.path.join(log_directory, 'logs/log.log')
LOG_FORMAT = "%(asctime)s - %(levelname)s %(name)s %(filename)s [line:%(lineno)d] - %(message)s"
rfh = logging.handlers.RotatingFileHandler(filename=logfile,encoding='UTF-8', maxBytes=1024*1024, backupCount=5) #按日志文件大小分割
sh = logging.StreamHandler() #控制台输出
logging.basicConfig(format=LOG_FORMAT,level=logging.DEBUG,handlers=[rfh,sh])
































import pytest
from django.test import TestCase
from blog.models import BlogCategory

@pytest.mark.django_db
class TestBlogCategory(TestCase):

    def setUp(self):
        self.blogcategory = BlogCategory.objects.create(id=1,title="Test Category", href='/category/1')

    def test_BogCategoryModel(self):
        blog_category = BlogCategory.objects.get(id=self.blogcategory.id)
        self.assertEqual(blog_category.title, "Test Category")
        self.assertEqual(blog_category.href, '/category/1')


@pytest.mark.django_db
def test_blog_category_create():
    blogcategory = BlogCategory.objects.create(id=1,title="Test Category", href='/category/1')
    category_count = BlogCategory.objects.count()
    assert category_count > 0, "Blog category was not created category_count=0."
    assert blogcategory.id > 0, "Blog category was not created."
    assert blogcategory.title == "Test Category", "Blog category title is wrong."
    assert blogcategory.href == "/category/1", "Blog category href is wrong."


@pytest.mark.django_db
def test_blog_category_query():
    category_count = len(BlogCategory.objects.all())
    assert category_count >= 0, "Blog category query error."


if __name__ == '__main__':
    pytest.main(["-s", "-v", "-p", "no:warnings", "--tb=short", "--html=report.html", "blog/tests.py"])


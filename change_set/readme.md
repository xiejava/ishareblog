## 1、base.py
base.py 用于解决sqlite3与django版本不兼容的问题。
解决方案： 
安装pysqlite3和pysqlite3-binary
- pip install pysqlite3
- pip install pysqlite3-binary

将venv\Lib\site-packages\django\db\backends\sqlite3\base.py文件中的找到 
 `from sqlite3 import dbapi2 as Database` 注释它，添加代码

`#from sqlite3 import dbapi2 as Database  #注释它`

`from pysqlite3 import dbapi2 as Database #新加这段代码`
